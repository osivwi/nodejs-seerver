import * as testSubject from '../src/x-module';

describe('Test notifications', () => {
  beforeEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should return a sum of 2 numbers', async () => {
    const actualResult = testSubject.sum(10, 10);

    expect(actualResult).toEqual(20);
  });
});
